window.onscroll = function() {myFunction()};

var header = document.getElementById("wrap_nav_menu");
var sticky = header.offsetTop;

function myFunction() {
  if (window.pageYOffset > sticky) {
    header.classList.add("nav_menu_sticky");
  } else {
    header.classList.remove("nav_menu_sticky");
  }
}